import { app as express } from './app';
import * as supertest from 'supertest';
import { MongoDatabase, Permission, User as Users } from '@ressourcesetvous/databases';

describe('Auth API', () => {

  let tester: supertest.SuperTest<supertest.Test>= null;
  let token = null;

  beforeAll(async () => {
    MongoDatabase.init()
    const app = express;
    tester = supertest(app)
  })

  it ('should start with healthcheck', (done) => {
    tester.get('/healthcheck')
    .expect('Content-Type', /json/)
    .expect(200, done)
  })

  it('shouldn\'t be possible to get users when not connected', (done) => {
    tester.get('/users').expect('Content-Type', /json/).expect(401, done)
  })

  it('should be able to login with good credentials', (done) => {
    tester
      .post('/login')
      .send(`username=${process.env.AUTH_TEST_USERNAME}`)
      .send(`password=${process.env.AUTH_TEST_PASSWORD}`)
      .expect('Content-Type', /json/)
      .expect((res) => {
        token = res.body.token
      })
      .expect(200, done)
  })
  it('should not be able to login with wrong credentials', (done) => {
    tester
      .post('/login')
      .send(`username=fpoeifsef`)
      .send(`password=poifeposif`)
      .expect('Content-Type', /text/)
      .expect(401, done)
  })

  it('should be possible to get users when connected and administrator', async () => {
    return tester.get('/users').set('Authorization', `Bearer ${token}`).expect('Content-Type', /json/).expect(200)
  })

})