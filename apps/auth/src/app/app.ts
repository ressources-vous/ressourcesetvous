import * as express from 'express';
import { json, urlencoded } from 'body-parser';
import { Users } from './user/route';
import { Login } from './login/route';
import { Permission } from './permission/route';

export const app = express()
  .use(json())
  .use(urlencoded({ extended: true }))
  .set('trust-proxy', 1)
  .get('/healthcheck', (req, res) => {
    const healthcheck = {
      uptime: process.uptime(),
      message: "OK",
      timestamp: Date.now(),
    };
    try {
      res.send(healthcheck);
    } catch (e) {
      healthcheck.message = e;
      res.status(503).send(healthcheck);
    }
  })
  .use('/login', Login())
  .use('/users', Users())
  .use('/permissions', Permission());