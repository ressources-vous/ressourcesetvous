import { User as Users, Connection as Connections } from '@ressourcesetvous/databases';
import { Roles, User, Connection, Permission } from '@ressourcesetvous/data-models';
import { verify } from 'jsonwebtoken'

export async function checkRequiredAuth(encoded_token: string, required: Roles | Permission, bypass_auth?: Roles) {
    const { token } = verify(encoded_token, process.env.AUTH_JWT_TOKEN) as { token: string };
    const connection: Connection = await Connections.findOne({ token });
    if (!connection) {
      throw 'Invalid auth for ' + token;
    }
    if (connection.ask_disconnect) {
      throw 'Asked disconnection for' + token;
    }
    const user: User = await Users.findById(connection.user_id).populate('permissions');
    if (!user) {
      throw `Can't find user :` + connection.user_id; 
    }
    if (Number.isInteger(required) || user.role >= (bypass_auth ?? Roles.Administrator)) {
      if(user.role >= (required as Roles)) {
        return {
          granted: true,
          user
        }
      }
      throw 'Not enough permissions to access ressources';
    }
    else if ((required as Permission)?.page) {
      const r = required as Permission;
      const permission = user.permissions.find(p => p.page === r.page)
      if (!permission) {
        throw 'Unauthorized to access : ' + r.page;
      }
      if(r.can_create && permission.can_create || r.can_read && permission.can_read || r.can_update && permission.can_update || r.can_delete && permission.can_delete) {
        return { 
          granted: true,
          user,
        }
      }
    }
    throw 'Not enough permissions to access ressources';
}