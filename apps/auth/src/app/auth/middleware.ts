import { Permission, Roles } from '@ressourcesetvous/data-models';
import { Request, Response, NextFunction } from 'express';
import { checkRequiredAuth } from './check';

type Payload = {
  required?: Roles,
  min?: Roles
}

export function authMiddleware(roles?: Payload) {
  return async (req: Request, res: Response, next: NextFunction) => {
    let r: Roles | Permission = roles.required;
    const route = req.baseUrl.replace('/', '');
    const token = req.get('Authorization')?.slice('Bearer '.length);
    if (!token) {
      return res.status(401).json({
        success: false,
        message: "NEED_AUTH"
      })
    }
    try {
      if (!r) {
        const method = req.method.toLowerCase();
        r = {
          page: route,
          can_create: method === 'post',
          can_read: method === 'get',
          can_update: method === 'put' || method === 'patch',
          can_delete: method === 'delete',
          _id: null,
          user_id: null
        }
      }
      const check = await checkRequiredAuth(token, r, roles.min);
      res.locals.user = check.user;
      return next();
    } catch(error) {
      return res.status(403).json({
        success: false,
        message: error
      });
    }
  }
}