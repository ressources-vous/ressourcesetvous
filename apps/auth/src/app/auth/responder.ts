import { checkRequiredAuth } from "./check"; 
import { Responder } from 'cote';
import { Permission } from "@ressourcesetvous/data-models";
import { AuthRequest, AUTH_QUERY } from '@ressourcesetvous/requesters';

  const responder = new Responder({ 
    name: 'Auth service',
    respondsTo: [AUTH_QUERY]
  })
  responder.on<AuthRequest & { type: string }>(AUTH_QUERY, async (req, res) => {
    try {
      const token = req.token;
      if (!token) {
        throw 'No token provided'
      }
      const method = req.method.toLowerCase();
      const required_permission: Permission = {
        page: req.route,
        can_create: method === 'post',
        can_read: method === 'get',
        can_update: method === 'put' || method === 'patch',
        can_delete: method === 'delete',
        _id: null,
        user_id: null
      }
      return checkRequiredAuth(token, req.required || required_permission, req.min_bypass);
    } catch(error) {
      res({
        granted: false,
        reason: error
      }, null);
    }
  })
