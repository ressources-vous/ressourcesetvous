import { User as Users, Connection as Connections } from '@ressourcesetvous/databases';
import { compare } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import { v4 as uuid } from 'uuid';
import { totp } from 'speakeasy';
import { Redis } from '@ressourcesetvous/databases';
import { toDataURL as QrCode } from 'qrcode';
import { User, Totp } from '@ressourcesetvous/data-models';
import { getLocation } from '@ressourcesetvous/geofinder';

export class LoginController {
  private redis = null;

  constructor() {
    this.redis = new Redis('TOTP');
  }

  async login(username: string, password: string, ip: string) {
    const user: User = await Users.findOne({ username }).exec();
    if (!user) {
      throw "WRONG_IDS";
    }
    const password_check = await compare(password, user.password);
    switch(2 * Number(password_check) + Number(user.ask_reset)) {
      case 0: // Wrong password and not reset
        throw "WRONG_IDS";
      case 1: // Wrong password but good tmp_password
        if (!(user.tmp_password && await compare(password, user.tmp_password))) {
          throw "WRONG_IDS";
        }
        await Users.findByIdAndUpdate(user._id, { tmp_password: null, ask_reset: false, password: user.tmp_password });
        break;
      case 2: // Good password
        break;
      case 3: // Good password and ask reset
        await Users.findByIdAndUpdate(user._id, { tmp_password: null, ask_reset: false });
        break;
      default:
        throw "WRONG_IDS";
    }
    if (user.totp === Totp.Enabled) {
      const tmp_id = uuid();
      this.redis.setCacheJSON(tmp_id, user._id);
      return {
        _id: tmp_id,
        message: "2FA VERIFICATION",
      };
    }
    return this.getConnexionToken(user._id, ip);
  }

  async verifyTotp(code: string, tmp_id: string, ip: string) {
    const _id = await this.redis.getCache(tmp_id);
    if (!_id) {
      throw "WRONG_IDS";
    }
    const user: User = await Users.findById(_id).exec();
    if (!user) {
      throw "WRONG_IDS";
    }
    const is_signed = totp.verify({
      secret: user.totp_secret,
      encoding: 'base32',
      token: code
    })

    if(!is_signed) {
      throw "WRONG_IDS";
    }
    return this.getConnexionToken(user._id, ip);
  }

  async getQrcode(_id: string) {
    const user: User = await Users.findById(_id);
    if (!user) {
      throw 'WRONG_IDS';
    }
    const image_url = process.env.TOTP_IMAGE_URL
    const totp_url = `otpauth://totp/Ressources&Vous:${user.username}?secret=${user.totp_secret}&image=${image_url}`;
    return await QrCode(totp_url);
  }

  private async getConnexionToken(_id: string, ip: string) {
    const sign_date = new Date();
    const expires_date = sign_date.getTime() + 1000 * 60 * 60 * 24 * 365 / 2;
    const token_unique = uuid();
    const token = sign({ token: token_unique, sign_date }, process.env.AUTH_JWT_TOKEN, { expiresIn: expires_date.toString() });
    if (!token) {
      throw 'TOKEN_SIGNING_ERROR';
    }
    const location = getLocation(ip);
    const connection = {
      ...location,
      user_id: _id,
      token: token_unique,
      ip,
      connection_date: sign_date,
      disconnection_date: new Date(expires_date)
    }
    const con = await new Connections(connection).save()
    await Users.collection.updateOne({ _id }, { $push: { connection_logs: con._id } });
    return {
      _id,
      token,
      sign_date,
      expires_date,
    };
  }
}