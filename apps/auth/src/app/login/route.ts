import { Router, Request, Response } from 'express'
import { LoginController } from './controller'


export const Login = () => {
  const controller = new LoginController()
  return Router()
    .post('/', async (req: Request, res: Response) => {
      try {
        const ip = "86.238.36.244";
        const user = await controller.login(req.body.username, req.body.password, ip)
        return res.status(200).json(user)
      } catch (error) {
        return res.status(401).send(error)
      }
    }).post('/2fa', async (req: Request, res: Response) => {
      try {
        const ip = req.get('X-Real-IP');
        const user = await controller.verifyTotp(req.body.code, req.body.id, ip)
        return res.status(200).json(user)
      } catch (error) {
        return res.status(401).send(error)
      }
    })
}