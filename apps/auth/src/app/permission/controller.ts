import { User as Users, Permission as Permissions, Redis } from '@ressourcesetvous/databases';
import { Permission } from '@ressourcesetvous/data-models';

export class PermissionController {

  private redis: Redis

  constructor() {
    this.redis = new Redis('Permissions');
  }

  async get(user_id: string) {
    const users: Permission[] = await Permissions.find({ user_id });
    return users;
  }

  async create(permission: Partial<Permission>) {
    const payload: Partial<Permission> = {
      ...permission,
      created_at: new Date(),
    };
    const created: Permission = await new Permissions(payload).save();
    Users.collection.updateOne({ _id: created.user_id }, { $push: { permission_ids: created._id } });
    return { success: true, permission: created };
  }

  async update(permission: Permission) {
    const payload: Permission = {
      ...permission,
      last_updated: new Date()
    };

    const updated: Permission = await Permissions.findByIdAndUpdate(permission._id, payload, {new: true}).exec();
    return { success: true, permission: updated };
  }
  
  async deletePermssion(id: string) {
    const permission: Permission = await Permissions.findById(id).exec();
    Users.collection.updateOne({ _id: permission.user_id }, { $pull: { permissions: permission._id } });
    this.redis.setCacheJSONWithExpire(`permission-${id}`, permission, 7200);
    Permissions.findByIdAndDelete(permission._id);
    return `permission-${id}`;
  }

  async reinsertDeletedPermission(permission_id: string) {
    const permission: Permission = await this.redis.getCacheJSON<Permission>(permission_id)
    Users.collection.updateOne({ _id: permission.user_id }, { $push: { permissions: permission._id } });
    await new Permissions(permission).save();
    return {
      success: true,
      reinserted: permission._id
    }
  }
}