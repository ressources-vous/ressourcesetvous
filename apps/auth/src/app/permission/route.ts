import { Router, Request, Response } from 'express'
import { PermissionController } from './controller'
import { authMiddleware } from '../auth/middleware'; 
import { Roles } from '@ressourcesetvous/data-models';

export const Permission = () => {
  const controller = new PermissionController()
  return Router()
    .get('/:id', authMiddleware({required: Roles.Administrator}), async (req: Request, res: Response) => {
      try {
        const permissions = await controller.get(req.params.id);
        res.status(200).json(permissions);
      } catch(error) {
        return res.status(500).send(error);
      }
    })
    .post('/', authMiddleware({required: Roles.Administrator}), async (req: Request, res: Response) => {
      try {
        const user = await controller.create(req.body)
        return res.status(201).json(user)
      } catch (error) {
        return res.status(500).send(error)
      }
    }).put('/', authMiddleware({required: Roles.Administrator}), async (req: Request, res: Response) => {
      try {
        const user = await controller.update(req.body)
        return res.status(201).json(user)
      } catch (error) {
        return res.status(500).send(error)
      }
    })
    .delete('/:id', authMiddleware({required: Roles.Administrator}), async (req: Request, res: Response) => {
      try {
        const user = await controller.deletePermssion(req.params.id)
        return res.status(200).json(user)
      } catch (error) {
        return res.status(401).send(error)
      }
    })
    .post('/recover', authMiddleware({required: Roles.Administrator}), async (req: Request, res: Response) => {
      try {
        const user = await controller.reinsertDeletedPermission(req.body.permisssion)
        return res.status(200).json(user)
      } catch (error) {
        return res.status(401).send(error)
      }
    })
}