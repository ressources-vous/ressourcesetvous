import { User as Users, Permission as Permissions, Redis } from '@ressourcesetvous/databases';
import { toDataURL as QrCode } from 'qrcode';
import { User, Permission, Totp } from '@ressourcesetvous/data-models';
import { hash, genSaltSync } from 'bcrypt';
import { generateSecret } from 'speakeasy';

export class UserController {

  private redis: Redis

  constructor() {
    this.redis = new Redis('user-delete');
  }

  async get() {
    const users: User[] = await Users.find({}, { password: 0, totp_secret: 0, tmp_password: 0});
    return users;
  }

  async getById(id: string) {
    const user: User = await Users.findById(id, { password: 0, totp_secret: 0, tmp_password: 0});
    return user;
  }

  async register(http_user: Partial<User>, creator: string) {
    const password = Math.random().toString(36).slice(-10);
    const hashed_password = await hash(password, genSaltSync(8));
    const new_user: Partial<User> = {
      ...http_user,
      created_at: new Date(),
      created_by: creator,
      password: hashed_password
    };
    const user: User = await new Users(new_user).save();
    // SEND IDS THROUGH MAIL

    return { success: true, _id: user._id };
  }

  async update(http_user: User) {
    if (http_user.password) {
      const hashed_password = await hash(http_user.password, genSaltSync(8));
      await Users.findByIdAndUpdate(http_user._id, {secret : hashed_password}).exec();
      delete http_user.password;
    }

    const updated_user: User = {
      ...http_user,
      last_updated: new Date()
    };

    if(http_user.totp === Totp.Enabled) {
      updated_user.totp_secret = generateSecret().base32;
    }
    const user: User = await Users.findByIdAndUpdate(http_user._id, updated_user, {new: true}).exec();
    return { success: true, user };
  }

  async getQrcode(uuid: string) {
    const _id = await this.redis.getCacheJSON(uuid, 'TOTP');
    const user: User = await Users.findById(_id);
    if (!user) {
      throw 'WRONG_IDS';
    }
    const image_url = process.env.TOTP_IMAGE_URL
    const totp_url = `otpauth://totp/Ressources&Vous:${user.username}?secret=${user.totp_secret}&image=${image_url}`;
    return await QrCode(totp_url);
  }
  
  async deleteUser(id: string) {
    const user = await await Users.findById(id).populate('permissions').exec();
    const user_id = `user-${id}`
    this.redis.setCacheJSONWithExpire(user_id, user, 7200);
    Users.findByIdAndDelete(id);
    const permissions = await Promise.all([...user.permissions].map(e => e._id).map(async _id => {
      const permission: Permission = await Permissions.findById(_id).exec();
      this.redis.setCacheJSONWithExpire(`permission-${_id}`, permission, 7200);
      Permissions.findByIdAndDelete(_id);
      return `permission-${_id}`;
    }));
    return {
      user: user_id,
      permissions
    }
  }

  async reinsertDeletedUser(user_id: string, permissions: string[]) {
    const user: User = await this.redis.getCacheJSON<User>(user_id)
    permissions.forEach(async (permission) => {
      const perm: Permission = await this.redis.getCacheJSON<Permission>(permission);
      new Permissions(perm).save();
    })
    await new Users(user).save();
    return {
      success: true,
      reinserted: user._id
    }
  }
}