import { Router, Request, Response, NextFunction } from 'express'
import { UserController } from './controller'
import { authMiddleware } from '../auth/middleware'; 
import { Roles } from '@ressourcesetvous/data-models';

export const Users = () => {
  const controller = new UserController()
  return Router()
    .get('/', authMiddleware({required: Roles.Administrator}), async (req: Request, res: Response) => {
      try {
        const users = await controller.get();
        res.status(200).json(users);
      } catch(error) {
        return res.status(500).send(error);
      }
    })
    .get('/by/:id', authMiddleware({required: Roles.User}), async (req: Request, res: Response) => {
      try {
        const user = await controller.getById(req.params.id);
        res.status(200).json(user);
      } catch(error) {
        return res.status(500).send(error);
      }
    })
    .get('/qr_code/:id', async (req: Request, res: Response) => {
      try {
        const qrcode = await controller.getQrcode(req.params.id);
        res.status(200).send(qrcode);
      } catch(error) {
        return res.status(500).send(error);
      }
    })
    .post('/register', authMiddleware({required: Roles.Administrator}), async (req: Request, res: Response) => {
      try {
        const id = res.locals.user._id;
        const user = await controller.register(req.body, id)
        return res.status(201).json(user)
      } catch (error) {
        return res.status(500).send(error)
      }
    }).put('/update/admin', authMiddleware({required: Roles.Administrator}), (req: Request, _: Response, next: NextFunction) => {
      const body = {...req.body}
      delete body.password;
      delete body.totp;
      delete body.totp_secret;
      delete body.connection_logs;
      delete body.created_at;
      delete body.created_by;
      next();
    }, async (req: Request, res: Response) => {
      try {
        const user = await controller.update(req.body)
        return res.status(201).json(user)
      } catch (error) {
        return res.status(500).send(error)
      }
    }).put('/update/user', authMiddleware({required: Roles.User}), (req: Request, _: Response, next: NextFunction) => {
      const body = {...req.body}
      delete body.permissions;
      delete body.role;
      delete body.connection_logs;
      delete body.created_at;
      delete body.created_by;
      next();
    }, async (req: Request, res: Response) => {
      try {
        const user = await controller.update(req.body)
        return res.status(201).json(user)
      } catch (error) {
        return res.status(500).send(error)
      }
    })
    .delete('/delete/:id', authMiddleware({required: Roles.Administrator}), async (req: Request, res: Response) => {
      try {
        const user = await controller.deleteUser(req.params.id)
        return res.status(200).json(user)
      } catch (error) {
        return res.status(401).send(error)
      }
    })
    .post('/recover_user', authMiddleware({required: Roles.Administrator}), async (req: Request, res: Response) => {
      try {
        const user = await controller.reinsertDeletedUser(req.body.user, req.body.permisssions)
        return res.status(200).json(user)
      } catch (error) {
        return res.status(401).send(error)
      }
    })
}