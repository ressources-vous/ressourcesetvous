global.Promise = require('bluebird');

import { createServer as http } from 'http';

import { MongoDatabase } from '@ressourcesetvous/databases';
import { app } from './app';

import './app/auth/responder';

const port = Number(process.env.AUTH_PORT) || 3002;
const host = process.env.AUTH_HOST || '0.0.0.0';

MongoDatabase.init().then(() => {
  const server = http(app).listen(port, host, () => {
    console.log(`Listening at http://${host}:${port}/`);
  });
  server.on('error', console.error);
}).catch(error => {
  console.error(error);
})