import { EmailTemplate } from '@ressourcesetvous/data-models';
import Email = require('email-templates')

export function sendEmail(
  template: EmailTemplate,
  to: string,
  replyTo: string,
  locals: any /* eslint-disable-line */,
  send = true
) {
    const email = new Email({
      message: {
        from: `Ressources&Vous <${process.env.MAIL_SERVER_USER}>`,
        to,
        replyTo,
      },
      send,
      transport: {
        host: process.env.MAIL_SERVER_HOST,
        port: Number(process.env.MAIL_SERVER_PORT),
        auth: {
          user: process.env.MAIL_SERVER_USER,
          pass: process.env.MAIL_SERVER_PASSWORD,
        },
      },
      views: {
        root: process.env.MAIL_DIRECTORY,
        options: {
          extension: 'hbs',
        },
      },
    });
    return email.send({ template, locals });
}