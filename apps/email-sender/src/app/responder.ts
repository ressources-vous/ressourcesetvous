import { Responder } from 'cote'
import { EMAIL_QUERY, EmailRequest, EmailResponse } from '@ressourcesetvous/requesters';

export class EmailResponder {
  private responder: Responder;

  constructor(listener: (data: EmailRequest) => Promise<EmailResponse>) {
    this.responder = new Responder({ name: 'Email Service', respondsTo: [EMAIL_QUERY]});
    this.responder.on<EmailRequest & { type: string }>(EMAIL_QUERY, (req: EmailRequest) => listener(req))
  }

  close() {
    this.responder.close()
  }
}