global.Promise = require('bluebird');

import { EmailResponder } from './app/responder';
import { sendEmail } from './app/email';

const responder = new EmailResponder(async (data) => {
  try {
    const emailsQueue = data.recipients.map(recipient => sendEmail(data.template, recipient, data.replyTo, data.data))
    const result = await Promise.all(emailsQueue);
    return {
      success: true,
      emails: result.map(email => email.messageId)
    }
  } catch(error) {
    console.error(error)
    return {
      success: false,
      emails: []
    }
  } 
});

process.on('SIGINT', () => {
  responder.close()
})