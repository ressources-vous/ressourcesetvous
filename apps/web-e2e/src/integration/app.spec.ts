describe('web', () => {
  beforeEach(() => cy.visit('/'));

  it('should display navbar', () => {
    cy.get('nav').should('be.visible');
  });
  
  it('should display dropdown when clicked', () => {
    cy.get('nav button').click();
    cy.get('nav .dropdown-list:last').should('not.be.visible');
    cy.get('nav .dropdown:last > a').click();
    cy.get('nav .dropdown-list:last').should('be.visible');
  });
});
