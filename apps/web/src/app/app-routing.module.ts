import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { IndexComponent } from './index/index.component'

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
    data: {
      title: 'Ressources&Vous',
      description: `La ressourcerie du canton de Rambouillet et d'Épernon`,
    },
  },
  {
    path: 'mentions-legales',
    loadChildren: () => import('./legals/legals.module').then(e => e.LegalsModule),
    data: {
      title: 'Mentions légales de Ressources&Vous',
      description: `Vous trouverez sur cette page toutes les informations légales regardant le site ressourcesetvous.org`
    }
  },
  {
    path: 'reglementation-generale-pour-la-protection-des-donnees',
    loadChildren: () => import('./rgpd/rgpd.module').then(e => e.RgpdModule),
    data: {
      title: 'Règlementation Générale pour la Protection des Données de Ressources&Vous',
      description: `Vous trouverez sur cette page toutes les informations concernant la collecte et l'utilisation de vos données sur le site ressourcesetvous.org`
    }
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      onSameUrlNavigation: 'reload',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
