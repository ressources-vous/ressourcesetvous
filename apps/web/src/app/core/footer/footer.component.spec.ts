import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ FooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contains 6 links', () => {
    const links = fixture.nativeElement.querySelectorAll('a');
    expect(links.length).toEqual(6);
  });

  it('should contains a facebook link', () => {
    const links: HTMLLinkElement[] = [...fixture.nativeElement.querySelectorAll('a')];
    expect(links.some(link => link?.href === 'https://www.facebook.com/RessourcesVous/?ref=br_rs')).toBeTruthy();
  });

  it('should contains an instagram link', () => {
    const links: HTMLLinkElement[] = [...fixture.nativeElement.querySelectorAll('a')];
    expect(links.some(link => link?.href === 'https://www.instagram.com/ressources__et__vous/?hl=fr')).toBeTruthy();
  });

  it('should contains the copyright with current year', () => {
    const year = new Date().getFullYear();
    const element = fixture.nativeElement.querySelector('.copyright');
    expect(element.textContent).toContain('Ressources&Vous ' + year.toString());
  });
});
