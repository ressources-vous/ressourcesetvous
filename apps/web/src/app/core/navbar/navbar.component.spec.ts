import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let compiled: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ NavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.nativeElement
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contains an image', () => {
    expect(compiled.querySelector('img')).toBeTruthy();
  });

  it ('should contains 11 links with no shop', () => {
    expect(compiled.querySelectorAll('a').length).toEqual(11);
  });

  it ('should contains 14 links with 3 shops', () => {
    component.places$ = of([{ _id: "a", title: "A", uuid: "a"}, { _id: "b", title: "B", uuid: "b"}, { _id: "c", title: "C", uuid: "c"}])
    fixture.detectChanges();
    expect(compiled.querySelectorAll('a').length).toEqual(14);
  });
  
});
