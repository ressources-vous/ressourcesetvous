import { isPlatformBrowser } from '@angular/common';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { Place } from '@ressourcesetvous/data-models';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'ressourcesetvous-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {

  navbarVisible = true;
  places$: Observable<Partial<Place>[]> = of([]);

  constructor(
    // eslint-disable-next-line
    @Inject(PLATFORM_ID) private platform: any,
  ) { }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platform)) {
      this.navbarVisible = window?.innerWidth > 1000;
    }
  }

  trackBy(i: number, place: Partial<Place>): string {
    return place._id ?? i.toString();
  }

}
