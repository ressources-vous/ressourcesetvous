import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { IndexAboutComponent } from './index-about.component';

describe('IndexAboutComponent', () => {
  let component: IndexAboutComponent;
  let fixture: ComponentFixture<IndexAboutComponent>;
  let element: HTMLElement

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexAboutComponent ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexAboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should contains a section title', () => {
    expect(element.querySelector('h2')?.textContent).toContain('Qui sommes nous ?');
  });

  it('should contains 1 cta link', () => {
    expect(element.querySelectorAll('a')?.length).toEqual(1);
  })
});
