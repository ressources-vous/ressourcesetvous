import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-index-about',
  templateUrl: './index-about.component.html',
  styleUrls: ['./index-about.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexAboutComponent {
}
