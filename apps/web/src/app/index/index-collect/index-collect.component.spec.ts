import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { IndexCollectComponent } from './index-collect.component';

describe('IndexCollectComponent', () => {
  let component: IndexCollectComponent;
  let fixture: ComponentFixture<IndexCollectComponent>;
  let element: HTMLElement

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexCollectComponent ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexCollectComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should contains section title <h2> tag', () => {
    expect(element.querySelector('h2')?.textContent).toEqual('Vous avez des objets encombrants à donner ?');
  });

  it('should contains a CTA link', () => {
    expect(element.querySelector('a')?.classList.contains('cta-link')).toBeTruthy();
  });
});
