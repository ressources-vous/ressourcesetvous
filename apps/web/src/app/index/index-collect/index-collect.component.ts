import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-index-collect',
  templateUrl: './index-collect.component.html',
  styleUrls: ['./index-collect.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexCollectComponent {}
