import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IndexContactComponent } from './index-contact.component';

describe('IndexContactComponent', () => {
  let component: IndexContactComponent;
  let fixture: ComponentFixture<IndexContactComponent>;
  let element: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexContactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexContactComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should contains a section title <h2> tag', () => {
    expect(element.querySelector('h2')?.textContent).toEqual('Vous êtes intéressés par notre démarche ?');
  });

  it('sould contains a phone link', () => {
    const links: HTMLLinkElement[] = [...fixture.nativeElement.querySelectorAll('a')];
    expect(links.find(link => link.href.startsWith('tel:'))?.textContent).toContain('09 51 23 59 31');
  });

  it('should contains a mail link', () => {
    const links: HTMLLinkElement[] = [...fixture.nativeElement.querySelectorAll('a')];
    expect(links.find(link => link.href.startsWith('mailto:'))?.textContent).toContain('accueil@ressourcesetvous.org');
  });
});
