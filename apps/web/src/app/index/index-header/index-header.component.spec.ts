import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexHeaderComponent } from './index-header.component';

describe('IndexHeaderComponent', () => {
  let component: IndexHeaderComponent;
  let fixture: ComponentFixture<IndexHeaderComponent>;
  let element: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    element = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display `Ressources&Vous` as big title', () => {
    expect(element.querySelector('h1#hero')?.textContent).toEqual('Ressources&Vous');
  });

  it('should display `Récup’, Répar’ et Compagnie` as subtitle', () => {
    expect(element.querySelector('p.hero-caption')?.textContent).toEqual('Récup’, Répar’ et Compagnie');
  });

  it ('should contains 1 link', () => {
    expect(element.querySelector('a')?.href).toContain('#ouvertures');
    expect(element.querySelector('a')?.textContent).toEqual('Sommes-nous ouvert ?');
  });
});
