import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-index-header',
  templateUrl: './index-header.component.html',
  styleUrls: ['./index-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexHeaderComponent {}
