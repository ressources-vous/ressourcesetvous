import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexPartnersComponent } from './index-partners.component';

describe('IndexPartnersComponent', () => {
  let component: IndexPartnersComponent;
  let fixture: ComponentFixture<IndexPartnersComponent>;
  let element: HTMLElement

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexPartnersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexPartnersComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contains a title', () => {
    expect(element.querySelector('h2')?.textContent).toContain('Nos partenaires');
  })
});
