import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-index-partners',
  templateUrl: './index-partners.component.html',
  styleUrls: ['./index-partners.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexPartnersComponent {}
