import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexPlacesComponent } from './index-places.component';

describe('IndexPlacesComponent', () => {
  let component: IndexPlacesComponent;
  let fixture: ComponentFixture<IndexPlacesComponent>;
  let element: HTMLElement

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexPlacesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexPlacesComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should contains main tag', () => {
    expect(element.querySelector('main')).toBeTruthy();
  });

  it('should contains a section title <h2> tag', () => {
    expect(element.querySelector('h2')?.textContent).toEqual('Sommes-nous ouvert ?');
  });

  it('should contains 2 section subtitle <h3> tag', () => {
    expect(element.querySelectorAll('h3').length).toEqual(2);
  });

  it('should contains shop opening dates in a <p> tag', () => {
    expect(element.querySelector('p')?.textContent).toEqual('Nos boutiques solidaires sont ouvertes les mercredis et les samedis de 10h à 18h');
  });
});
