import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-index-places',
  templateUrl: './index-places.component.html',
  styleUrls: ['./index-places.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexPlacesComponent {}
