import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IndexAboutComponent } from './index-about/index-about.component';
import { IndexCollectComponent } from './index-collect/index-collect.component';
import { IndexContactComponent } from './index-contact/index-contact.component';
import { IndexHeaderComponent } from './index-header/index-header.component';
import { IndexPartnersComponent } from './index-partners/index-partners.component';
import { IndexPlacesComponent } from './index-places/index-places.component';

import { IndexComponent } from './index.component';

describe('IndexComponent', () => {
  let component: IndexComponent;
  let fixture: ComponentFixture<IndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        IndexComponent,
        IndexHeaderComponent,
        IndexAboutComponent,
        IndexPlacesComponent,
        IndexCollectComponent,
        IndexContactComponent,
        IndexPartnersComponent
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
