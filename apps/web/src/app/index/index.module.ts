import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexHeaderComponent } from './index-header/index-header.component';
import { IndexComponent } from './index.component';
import { IndexAboutComponent } from './index-about/index-about.component';
import { IndexPlacesComponent } from './index-places/index-places.component';
import { IndexCollectComponent } from './index-collect/index-collect.component';
import { IndexContactComponent } from './index-contact/index-contact.component';
import { IndexPartnersComponent } from './index-partners/index-partners.component';

@NgModule({
  declarations: [
    IndexHeaderComponent,
    IndexComponent,
    IndexAboutComponent,
    IndexPlacesComponent,
    IndexCollectComponent,
    IndexContactComponent,
    IndexPartnersComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    IndexComponent
  ]
})
export class IndexModule { }
