import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-legal-hosting',
  templateUrl: './hosting.component.html',
  styleUrls: ['./hosting.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HostingComponent {}
