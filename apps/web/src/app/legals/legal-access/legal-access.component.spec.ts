import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalAccessComponent } from './legal-access.component';

describe('LegalAccessComponent', () => {
  let component: LegalAccessComponent;
  let fixture: ComponentFixture<LegalAccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LegalAccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
