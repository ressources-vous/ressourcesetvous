import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-legal-access',
  templateUrl: './legal-access.component.html',
  styleUrls: ['./legal-access.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LegalAccessComponent {}
