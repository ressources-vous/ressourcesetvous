import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalCookiesComponent } from './legal-cookies.component';

describe('LegalCookiesComponent', () => {
  let component: LegalCookiesComponent;
  let fixture: ComponentFixture<LegalCookiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LegalCookiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalCookiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
