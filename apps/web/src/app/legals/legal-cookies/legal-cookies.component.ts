import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-legal-cookies',
  templateUrl: './legal-cookies.component.html',
  styleUrls: ['./legal-cookies.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LegalCookiesComponent {}
