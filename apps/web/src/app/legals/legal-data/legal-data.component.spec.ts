import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalDataComponent } from './legal-data.component';

describe('LegalDataComponent', () => {
  let component: LegalDataComponent;
  let fixture: ComponentFixture<LegalDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LegalDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
