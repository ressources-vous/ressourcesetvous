import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-legal-data',
  templateUrl: './legal-data.component.html',
  styleUrls: ['./legal-data.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LegalDataComponent {}
