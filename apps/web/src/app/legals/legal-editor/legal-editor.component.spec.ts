import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalEditorComponent } from './legal-editor.component';

describe('LegalEditorComponent', () => {
  let component: LegalEditorComponent;
  let fixture: ComponentFixture<LegalEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LegalEditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
