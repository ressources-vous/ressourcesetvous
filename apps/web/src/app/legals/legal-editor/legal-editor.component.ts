import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-legal-editor',
  templateUrl: './legal-editor.component.html',
  styleUrls: ['./legal-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LegalEditorComponent {}
