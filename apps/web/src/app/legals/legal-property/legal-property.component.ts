import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-legal-property',
  templateUrl: './legal-property.component.html',
  styleUrls: ['./legal-property.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LegalPropertyComponent {}
