import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HeaderComponent } from './header/header.component';
import { HostingComponent } from './hosting/hosting.component';
import { LegalAccessComponent } from './legal-access/legal-access.component';
import { LegalCookiesComponent } from './legal-cookies/legal-cookies.component';
import { LegalDataComponent } from './legal-data/legal-data.component';
import { LegalEditorComponent } from './legal-editor/legal-editor.component';
import { LegalPropertyComponent } from './legal-property/legal-property.component';

import { LegalsComponent } from './legals.component';

describe('LegalsComponent', () => {
  let component: LegalsComponent;
  let fixture: ComponentFixture<LegalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        LegalsComponent,
        HeaderComponent,
        LegalEditorComponent,
        LegalAccessComponent,
        LegalDataComponent,
        LegalCookiesComponent,
        LegalPropertyComponent,
        HostingComponent,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
