import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-legals',
  templateUrl: './legals.component.html',
  styleUrls: ['./legals.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LegalsComponent {}
