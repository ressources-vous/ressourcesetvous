import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { LegalEditorComponent } from './legal-editor/legal-editor.component';
import { LegalAccessComponent } from './legal-access/legal-access.component';
import { LegalDataComponent } from './legal-data/legal-data.component';
import { LegalCookiesComponent } from './legal-cookies/legal-cookies.component';
import { LegalPropertyComponent } from './legal-property/legal-property.component';
import { LegalsComponent } from './legals.component';
import { RouterModule } from '@angular/router';
import { HostingComponent } from './hosting/hosting.component';

@NgModule({
  declarations: [
    LegalsComponent,
    HeaderComponent,
    LegalEditorComponent,
    LegalAccessComponent,
    LegalDataComponent,
    LegalCookiesComponent,
    LegalPropertyComponent,
    HostingComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '', component: LegalsComponent
    }]),
  ]
})
export class LegalsModule { }
