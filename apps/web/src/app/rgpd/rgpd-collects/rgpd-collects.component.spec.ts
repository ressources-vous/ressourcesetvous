import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RgpdCollectsComponent } from './rgpd-collects.component';

describe('RgpdCollectsComponent', () => {
  let component: RgpdCollectsComponent;
  let fixture: ComponentFixture<RgpdCollectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RgpdCollectsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RgpdCollectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
