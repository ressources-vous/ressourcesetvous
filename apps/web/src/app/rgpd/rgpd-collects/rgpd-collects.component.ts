import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-rgpd-collects',
  templateUrl: './rgpd-collects.component.html',
  styleUrls: ['./rgpd-collects.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RgpdCollectsComponent {}
