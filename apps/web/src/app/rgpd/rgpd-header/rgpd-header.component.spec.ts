import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RgpdHeaderComponent } from './rgpd-header.component';

describe('RgpdHeaderComponent', () => {
  let component: RgpdHeaderComponent;
  let fixture: ComponentFixture<RgpdHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RgpdHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RgpdHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
