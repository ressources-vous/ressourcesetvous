import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-rgpd-header',
  templateUrl: './rgpd-header.component.html',
  styleUrls: ['./rgpd-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RgpdHeaderComponent {}
