import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RgpdMessagesComponent } from './rgpd-messages.component';

describe('RgpdMessagesComponent', () => {
  let component: RgpdMessagesComponent;
  let fixture: ComponentFixture<RgpdMessagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RgpdMessagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RgpdMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
