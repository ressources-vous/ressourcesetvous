import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-rgpd-messages',
  templateUrl: './rgpd-messages.component.html',
  styleUrls: ['./rgpd-messages.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RgpdMessagesComponent {}
