import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RgpdNewsletterComponent } from './rgpd-newsletter.component';

describe('RgpdNewsletterComponent', () => {
  let component: RgpdNewsletterComponent;
  let fixture: ComponentFixture<RgpdNewsletterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RgpdNewsletterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RgpdNewsletterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
