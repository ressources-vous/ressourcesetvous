import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-rgpd-newsletter',
  templateUrl: './rgpd-newsletter.component.html',
  styleUrls: ['./rgpd-newsletter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RgpdNewsletterComponent{}
