import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RgpdCollectsComponent } from './rgpd-collects/rgpd-collects.component';
import { RgpdHeaderComponent } from './rgpd-header/rgpd-header.component';
import { RgpdMessagesComponent } from './rgpd-messages/rgpd-messages.component';
import { RgpdNewsletterComponent } from './rgpd-newsletter/rgpd-newsletter.component';
import { RgpdComponent } from './rgpd.component';

describe('RgpdComponent', () => {
  let component: RgpdComponent;
  let fixture: ComponentFixture<RgpdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        RgpdComponent,
        RgpdHeaderComponent,
        RgpdCollectsComponent,
        RgpdMessagesComponent,
        RgpdNewsletterComponent
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RgpdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
