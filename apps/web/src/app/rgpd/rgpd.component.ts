import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ressourcesetvous-rgpd',
  templateUrl: './rgpd.component.html',
  styleUrls: ['./rgpd.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RgpdComponent{}
