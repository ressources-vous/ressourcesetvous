import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RgpdComponent } from './rgpd.component';
import { RgpdHeaderComponent } from './rgpd-header/rgpd-header.component';
import { RgpdCollectsComponent } from './rgpd-collects/rgpd-collects.component';
import { RgpdMessagesComponent } from './rgpd-messages/rgpd-messages.component';
import { RgpdNewsletterComponent } from './rgpd-newsletter/rgpd-newsletter.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    RgpdComponent,
    RgpdHeaderComponent,
    RgpdCollectsComponent,
    RgpdMessagesComponent,
    RgpdNewsletterComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '', component: RgpdComponent
    }])
  ]
})
export class RgpdModule { }
