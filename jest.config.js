module.exports = {
  projects: [
    '<rootDir>/apps/web',
    '<rootDir>/libs/ui',
    '<rootDir>/libs/data-models',
    '<rootDir>/apps/auth',
    '<rootDir>/libs/databases',
    '<rootDir>/libs/geofinder',
    '<rootDir>/libs/requesters',
    '<rootDir>/apps/email-sender',
  ],
  reporters: ['jest-teamcity'],
};
