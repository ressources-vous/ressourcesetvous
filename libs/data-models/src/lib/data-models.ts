import { Roles, Totp } from "./enums";

export interface Place {
  _id: string
  uuid: string
  title: string
  test: string
  place_type: 'shop' | 'depot'
  is_open: boolean
  address: Address
  image: File
  pictures: Picture[]
  created_at: Date
  updated_at: Date
  __v: number
}

export interface Address {
  _id: string
  street: string
  zip: string
  city: string
  lat: string
  lng: string
  created_at: Date
  updated_at: Date
  __v: number
}

export interface File {
  _id: string
  media_type: 'image' | 'video'
  legacy_original: string
  webp_original: string
  legacy_600: string
  webp_600: string
  legacy_300: string
  webp_300: string
  legacy_200: string
  webp_200: string
  created_at: Date
  updated_at: Date
  __v: number
}

export interface Picture {
  _id: number
  title?: string
  file: File
  created_at: Date
  updated_at: Date
  __v: number
}


export interface User {
  _id: string,
  username: string,
  email: string,
  verified: true,
  role: Roles
  created_at: Date,
  created_by: string
  last_updated: Date
  ask_reset: boolean,
  connection_logs: string[]
  connections?: Connection[]
  send_email_on_connection: boolean
  permission_ids: string[]
  permissions?: Permission[]
  totp: Totp,
  totp_secret?: string
  password?: string
  tmp_password?: string
}

export interface Connection {
  _id: string
  user_id: string
  user?: User
  token: string
  connection_date: Date
  ip: string
  country: string
  region: string
  lat: string
  lng: string
  zip: string
  timezone: string
  ask_disconnect: boolean
  disconnection_date: boolean
}

export interface Permission {
  _id: string,
  page: string,
  can_read: boolean
  can_create: boolean
  can_update: boolean
  can_delete: boolean
  user_id: string
  user?: User
  created_at?: Date
  last_updated?: Date
}