export enum Roles {
  Visitor,
  User,
  Editor,
  Administrator,
  Supervisor
}

export enum Totp {
  None,
  Enabled,
  Dont_Ask_Again
}

export enum EmailTemplate {
  INVITE = 'invitation',
  RESET_PASSWORD = 'reset-password',
  COLLECT = 'collect',
  CONTACT = 'contact',
  RESERVATION = 'reservation',
  UNSUBSCRIBE = 'unsubscribe',
  EVENT = 'events',
  AUTH = 'authentification'
}