import { Schema, model, Types } from "mongoose";
import * as unique_validator from 'mongoose-unique-validator';

import { Connection as Connection_DAO } from '@ressourcesetvous/data-models';

const schema = new Schema({
  user_id: {
    type: Types.ObjectId,
    required: true,
    unique: false,
    maxLength: 32
  },
  token: {
    type: String,
    required: true,
    unique: true,
    maxLength: 36
  },
  connection_date: {
    type: Date,
    required: true
  },
  ip: {
    type: String,
    required: true,
  },
  country: {
    type: String,
    required: true,
    maxLength: 2
  },
  region: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  timezone: {
    type: String,
    required: true
  },
  lat: {
    type: String,
    required: true
  },
  lng: {
    type: String,
    required: true
  },
  ask_disconnection: {
    type: Boolean,
    required: true,
    default: false
  },
  disconnection_date: {
    type: Date,
    required: false
  }
})

schema.virtual('user',{
  ref: 'Users',
  localField: 'user_id',
  foreignField: '_id',
  justOne: true
});

schema.set('toObject', { virtuals: true });
schema.set('toJSON', { virtuals: true });

schema.plugin(unique_validator);

export const Connection = model<Connection_DAO>('Connections', schema)