import { Schema, model, Types } from 'mongoose';
import * as unique_validator from 'mongoose-unique-validator';

import { Permission as Permission_DAO } from '@ressourcesetvous/data-models';

const schema = new Schema({
  page: {
    type: String,
    required: true
  },
  can_read: {
    type: Boolean,
    default: true,
    required: true
  },
  can_create: {
    type: Boolean,
    default: false,
    required: true
  },
  can_update: {
    type: Boolean,
    default: false,
    required: true
  },
  can_delete: {
    type: Boolean,
    default: false,
    required: true
  },
  user_id: {
    type: Types.ObjectId,
    required: true
  },
  created_at: {
    type: Date,
    required: true
  },
  last_updated: {
    type: Date,
    required: false
  },
});

schema.virtual('user',{
  ref: 'Users',
  localField: 'user_id',
  foreignField: '_id',
  justOne: true
});

schema.set('toObject', { virtuals: true });
schema.set('toJSON', { virtuals: true });

schema.plugin(unique_validator);

export const Permission = model<Permission_DAO>('Permissions', schema);
