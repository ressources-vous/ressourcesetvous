import { Schema, model, Types } from 'mongoose';
import * as unique_validator from 'mongoose-unique-validator';

import { Roles, Totp, User as User_DAO } from '@ressourcesetvous/data-models';

const schema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true, 
    maxLength: 30
  },
  password: {
    type: String,
    required: true,
    maxLength: 64
  },
  email: {
    type: String,
    required: true,
    unique: true,
    validate: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  },
  verified: {
    type: Boolean,
    required: true,
    default: false
  },
  role: {
    type: Number,
    required: true,
    enum: Roles,
    default: Roles.User
  },
  created_at: {
    type: Date,
    required: true
  },
  created_by: {
    type: Types.ObjectId,
    ref: "Users",
    autopopulate: false
  },
  last_updated: {
    type: Date,
    required: false
  },
  ask_reset: {
    type: Boolean,
    default: false
  },
  tmp_password: {
    type: String,
    required: false,
    maxLength: 64
  },
  connection_logs: {
    type: [Types.ObjectId],
    ref: "Connections",
    default: []
  },
  send_email_on_connection: {
    type: Boolean,
    default: true
  },
  permission_ids: {
    type: [Types.ObjectId],
    ref: "Permissions",
  },
  totp: {
    type: Number,
    enum: Totp,
    default: Totp.None
  },
  totp_secret: {
    type: String,
    required: false
  }
})

schema.virtual('permissions',{
  ref: 'Permissions',
  localField: 'permission_ids',
  foreignField: '_id',
});

schema.virtual('connections',{
  ref: 'Connections',
  localField: 'connection_logs',
  foreignField: '_id',
});

schema.set('toObject', { virtuals: true });
schema.set('toJSON', { virtuals: true });

schema.plugin(unique_validator);

export const User = model<User_DAO>('Users', schema);