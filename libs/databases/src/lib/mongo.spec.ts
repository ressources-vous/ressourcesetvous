import { MongoDatabase } from './mongo';

describe('MongoDB connection checker', () => {
  it('should create connection', () => {
    return MongoDatabase.init().then((connexion) => {
      expect(connexion).toBeDefined();
    }).catch((error) => {
      throw error;
    });
  });
});