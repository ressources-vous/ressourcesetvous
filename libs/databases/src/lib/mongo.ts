global.Promise = require('bluebird');

import { connect, ConnectOptions } from 'mongoose'

export class MongoDatabase {
  private static options: ConnectOptions = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    autoIndex: false,
    poolSize: 10,
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000,
    socketTimeoutMS: 45000,
    useUnifiedTopology: true,
  }

  static init() {
    const host = process.env.MONGO_HOST || 'mongo.grk';
    const port = process.env.MONGO_PORT || '27017';
    const name = process.env.MONGO_NAME || 'ressourcesetvous';
    return connect(`mongodb://${host}:${port}/${name}`, this.options)
  }
}