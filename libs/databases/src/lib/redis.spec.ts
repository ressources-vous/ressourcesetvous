import { Redis } from './redis';

describe('REDIS', () => {
  let client = null;

  beforeEach(() => {
    client = new Redis('test');
  })

  afterAll(() => {
    client.close(true);
  })

  it('should have a connection', () => {
    expect(client.redisClient).toBeDefined();
  });

  it('should contains a prefix', () => {
    expect(client.getPrefix()).toEqual('test');
  });
  it('should be able to change prefix', () => {
    client.setPrefix('test2');
    expect(client.getPrefix()).toEqual('test2');
  });

  it('should be able to store data in redis', () => {
    return client.setCache('dumb', 'hello world').then((response) => {
      expect(response).toContain('OK');
    })
  });

  it('should be able to get data from redis', () => {
    return client.setCache('dumb', 'hello world')
      .then(() => client.getCache('dumb'))
      .then((response) => {
        expect(response).toContain('hello world');
    })
  });

  it('should be able to delete data from redis', () => {
    return client.setCache('dumb', 'hello world')
      .then(() => client.deleteCache('dumb'))
      .then((response) => {
        expect(response).toBeDefined();
    })
  });

});