global.Promise = require('bluebird');

import { promisify } from 'bluebird';
import { createClient, RedisClient } from 'redis';

export class Redis {

  redisClient: RedisClient;
  private get = null;
  private set = null;
  private hmset = null;
  private del = null;
  private prefix: string;

  constructor(prefix: string) {
    this.prefix = prefix;
    this.redisClient = createClient({
      host: process.env.REDIS_HOST,
      password: process.env.REDIS_PASSWORD
    })

    this.get = promisify(this.redisClient.get).bind(this.redisClient);
    this.set = promisify(this.redisClient.set).bind(this.redisClient);
    this.hmset = promisify(this.redisClient.hmset).bind(this.redisClient);
    this.del = promisify(this.redisClient.del).bind(this.redisClient);
  }

  getPrefix() {
    return this.prefix;
  }

  setPrefix(prefix: string) {
    this.prefix = prefix;
  }

  formatKey(key: string, prefix?: string) {
    return `[${prefix || this.prefix}] ${key}`;
  }

  async getCache(key: string, prefix?: string): Promise<string> {
    return await this.get(this.formatKey(key, prefix));
  }
  async getCacheJSON<T>(key: string, prefix?: string): Promise<T> {
    return JSON.parse(await this.getCache(key, prefix));
  }

  async setCache(key: string, value: string, prefix?: string) {
    return await this.set(this.formatKey(key, prefix), value);
  }
  async setCacheJSON<T>(key: string, value: T, prefix?: string) {
    return await this.setCache(key, JSON.stringify(value), prefix);
  }

  async setCacheWithExpire(key: string, value: string, expire: number, prefix?:string) {
    const qKey = this.formatKey(key, prefix);
    return await this.hmset(`
      MULTI
      SET ${qKey} "${value}"
      EXPIRE ${qKey} ${expire}
      EXEC
    `)
  }

  async setCacheJSONWithExpire<T>(key: string, value: T, expire: number, prefix?: string) {
    return await this.setCacheWithExpire(key, JSON.stringify(value), expire, prefix);
  }

  async deleteCache(key: string, prefix?: string) {
    return this.del(this.formatKey(key, prefix));
  }

  close(flush = false) {
    return this.redisClient.end(flush);
  }
}