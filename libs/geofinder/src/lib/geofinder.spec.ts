import { getLocation, convertLookupToRVAddress } from './geofinder';

describe('geofinder', () => {

  it('should parse request to proper format', () => {
    const rawAdress = {
      country: 'FR',
      region: 'Ile-de-France',
      city: 'Paris',
      ll: [0, 0],
      timezone: 'Europe/Paris'
    };
    const parsed = convertLookupToRVAddress(rawAdress)
    expect(parsed).toEqual({
      country: 'FR',
      region: 'Ile-de-France',
      city: 'Paris',
      lat: 0,
      lng: 0,
      timezone: 'Europe/Paris'
    });
  });

  it('should display ip address location', () => {
    const ip = "86.238.36.244";
    const loc = getLocation(ip)
    expect(loc.country).toEqual('FR')
    expect(loc.timezone).toEqual('Europe/Paris')
    expect(loc.region).toEqual('IDF')
  });

  it('should handle local ips properly', () => {
    const ip = "192.168.1.56";
    const loc = getLocation(ip);
    expect(loc).toEqual({
      country: 'FR',
      region: 'Ile-de-France',
      city: 'Rambouillet',
      lat: 0,
      lng: 0,
      timezone: 'Europe/Paris'
    })
  });

  it('should be able to return value with IPv6', () => {
    const ip = "2001:4860:4860::8844";
    const loc = getLocation(ip);
    expect(loc).toEqual({
      country: 'US',
      region: '',
      city: '',
      lat: 37.751,
      lng: -97.822,
      timezone: 'America/Chicago'
    })
  }) 
});
