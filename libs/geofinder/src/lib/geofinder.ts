import { lookup } from 'geoip-lite';

type RawLookup = {
  country: string,
  region: string,
  city: string,
  ll: number[],
  timezone: string
}

export function getLocation(ip: string) {
  let loc: RawLookup = lookup(ip);
  if (!loc) {
    loc = {
      country: 'FR',
      region: 'Ile-de-France',
      city: 'Rambouillet',
      ll: [0, 0],
      timezone: 'Europe/Paris'
    }
  }
  return convertLookupToRVAddress(loc);
}

export function convertLookupToRVAddress(lookup: RawLookup): Location {
  return {
    country: lookup.country,
    region: lookup.region,
    city: lookup.city,
    lat: lookup.ll[0],
    lng: lookup.ll[1],
    timezone: lookup.timezone
  }
}

export interface Location {
  country: string
  region: string
  timezone: string
  city: string
  lat: number
  lng: number
}