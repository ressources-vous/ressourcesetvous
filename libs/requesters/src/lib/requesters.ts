import * as Payloads from "./requesters_payloads";
import { Requester } from "cote";

export class IAC {
  private requester: Requester;

  constructor(name: string) {
    this.requester = new Requester({ name });
  }

  getAuth(payload: Payloads.AuthRequest): Promise<Payloads.AuthResponse> {
    return this.requester.send<Payloads.AuthRequest & { type: string }>({type: Payloads.AUTH_QUERY, ...payload })
  }

  sendEmail(payload: Payloads.EmailRequest): Promise<Payloads.EmailResponse> {
    return this.requester.send<Payloads.EmailRequest & { type: string }>({type: Payloads.EMAIL_QUERY, ...payload })
  }

  processFile(payload: Payloads.FileProcessingRequest): Promise<Payloads.FileProcessingResponse> {
    return this.requester.send<Payloads.FileProcessingRequest & { type: string}>({type: Payloads.FILE_PROCESSING_QUERY, ...payload})
  }

  close() {
    this.requester.close();
  }
}