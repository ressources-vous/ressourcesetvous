import { EmailTemplate, Roles, User } from "@ressourcesetvous/data-models";

export const AUTH_QUERY = "IS_USER_ALLOWED";
export interface AuthRequest {
  required?: Roles
  min_bypass?: Roles
  route: string
  token: string
  method: string
}
export type AuthResponse = {
  granted: boolean,
  user: User
}

export const EMAIL_QUERY = "SEND_EMAIL";
export interface EmailRequest {
  template: EmailTemplate
  recipients: string[]
  replyTo:string
  data: any // eslint-disable-line
  attachments?: (string | File)[]
}
export type EmailResponse = {
  success: boolean,
  emails: string[]
}

export const FILE_PROCESSING_QUERY = "FILES_PROCESSING";
export interface FileProcessingRequest {
  uuid: string
  destination: string
  folder: string
  processor: 'IMAGE' | 'VIDEO'
  legacy: 'jpg' | 'png' | 'mp4'
  formats: { width: number, height: number }[]
}
export type FileProcessingResponse = {
  success: boolean,
  uuid: string
  files: string[]
}